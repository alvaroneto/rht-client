import React, { Component } from 'react';
import { Route, Redirect } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { login } from '../../../actions/Auth/index';

class Login extends Component {
  constructor (props){
    super(props);
    this.state = {
      formData: {
        username: '',
        passwd: ''
      }
    }
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
      let formData = this.state.formData;
      formData[event.target.id] = event.target.value
      this.setState({formData: formData});
  }
  
  handleSubmit = () => {
    return this.props.login(this.state.formData.username, this.state.formData.passwd)
    .then((success) => {
      window.location.replace("/");
    })
    .catch((err) => {
      alert(err)
    })
  }
  
  render() { 
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="card-group mb-0">
              <div className="card p-2">
                <div className="card-block">
                  <h1>Login</h1>
                  <p className="text-muted">Sign In to your account</p>
                  <div className="input-group mb-1">
                    <span className="input-group-addon"><i className="icon-user"></i></span>
                    <input name="username" id="username" type="text" className="form-control" placeholder="Username" onChange={this.handleChange}/>
                  </div>
                  <div className="input-group mb-2">
                    <span className="input-group-addon"><i className="icon-lock"></i></span>
                    <input name="passwd" id="passwd" type="password" className="form-control" placeholder="Password" onChange={this.handleChange}/>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <button type="button" className="btn btn-primary px-2" onClick={this.handleSubmit}>Login</button>
                    </div>
                    <div className="col-6 text-right">
                      <button type="button" className="btn btn-link px-0">Forgot password?</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-inverse card-primary py-3 hidden-md-down" style={{ width: 44 + '%' }}>
                <div className="card-block text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <button type="button" className="btn btn-primary active mt-1">Register Now!</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators({ login: login }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Login);
