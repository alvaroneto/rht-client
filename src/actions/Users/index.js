import request from 'axios';

var instance = request.create({
  baseURL: 'http://localhost:1337/v1'
});

instance.headers = {
    "Cache-Control": "no-cache",
    "Access-Control-Allow-Methods": "DELETE, HEAD, GET, OPTIONS, POST, PUT",
    "Access-Control-Allow-Headers": "Content-Type, Content-Range, Content-Disposition, Content-Description",
    "Access-Control-Max-Age": "1728000",
    "Content-Type": "application/json"
}

export function loadUsersList () {
    return {
        type: 'FETCH_USERS',
        payload: new Promise((resolve, reject) => {
            instance.get('/user')
            .then(function (response) {
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function getPermissions () {
    return {
        type: 'FETCH_PERMISSIONS',
        payload: new Promise((resolve, reject) => {
            instance.get('/permission')
            .then(function (response) {
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function getPositions () {
    return {
        type: 'FETCH_POSITIONS',
        payload: new Promise((resolve, reject) => {
            instance.get('/position')
            .then(function (response) {
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function createUser (data) {
    return {
        type: 'CREATE_USER',
        payload: new Promise((resolve, reject) => {
            instance.post('/user', data)
            .then(function (response) {
                return resolve(response);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function updateUser (data) {
    return {
        type: 'UPDATE_USER',
        payload: new Promise((resolve, reject) => {
            instance.put('/user/'+data.id, data)
            .then(function (response) {
                return resolve(response);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function removeUsers (data) {
    return {
        type: 'REMOVE_USERS',
        payload: new Promise((resolve, reject) => {
            instance({
                method: 'delete',
                url: '/user',
                data: { id: data }
            })
            .then(function (response) {
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export const selectUser = (user) => {
    return {
        type: 'USER_SELECTED',
        payload: user
    }
}