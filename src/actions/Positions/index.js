import request from 'axios';

var instance = request.create({
  baseURL: 'http://localhost:1337/v1'
});

instance.headers = {
    "Cache-Control": "no-cache",
    "Access-Control-Allow-Methods": "DELETE, HEAD, GET, OPTIONS, POST, PUT",
    "Access-Control-Allow-Headers": "Content-Type, Content-Range, Content-Disposition, Content-Description",
    "Access-Control-Max-Age": "1728000",
    "Content-Type": "application/json"
}

export function getPermissions () {
    return {
        type: 'FETCH_PERMISSIONS',
        payload: new Promise((resolve, reject) => {
            instance.get('/permission')
            .then(function (response) {
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}

export function getPositions () {
    return {
        type: 'FETCH_POSITIONS',
        payload: new Promise((resolve, reject) => {
            instance.get('/position')
            .then(function (response) {
                console.log('++++++')
                console.log(response.data)
                console.log('++++++')
                return resolve(response.data);
            })
            .catch(function (error) {
                return reject(error);
            })
        })
    }
}