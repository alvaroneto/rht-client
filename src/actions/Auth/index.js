import React from 'react'
import {bindActionCreators} from 'redux'
import request from 'axios'
import { connect } from 'react-redux'
import querystring from 'query-string'
import react_cookie from 'react-cookie'
import node_cookie from 'cookie'
import _ from 'lodash'
import setAuthorization from '../../utils/setAuthorizationToken'
import jwtDecode from 'jwt-decode';
import { SET_CURRENT_USER } from '../types';

const config = {
  url: 'http://localhost:1337/',
  api: 'v1',
  urlToken: 'oauth/token',
  client_id:'H8V03B0Z4F',
  client_secret: 'RGlwtgQeyw9ubTqQYcBjm7iTaeFkiH'
}

var instance = request.create({
  baseURL: config.url
});

instance.headers = {
    "Cache-Control": "no-cache",
    "Access-Control-Allow-Methods": "DELETE, HEAD, GET, OPTIONS, POST, PUT",
    "Access-Control-Allow-Headers": "Content-Type, Content-Range, Content-Disposition, Content-Description",
    "Access-Control-Max-Age": "1728000",
    "Content-Type": "application/json",
}

function fetchUser(token) {
  return new Promise((resolve, reject) => {
    request.get(`${config.url}${config.api}/user`, {
      headers:{
        'Authorization':`Bearer ${localStorage.access_token}`
      }
    }).then(res => {
      resolve(res);
    }).catch(e => {
      reject(e)
    });
  })
}

function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    user
  };
}

module.exports = {
  cancel: () => {
    return {
      type: 'OAUTH_CANCELED'
    }
  },
  error: (error) => {
    return {
      type: 'OAUTH_ERROR',
      payload: error,
    }
  },
  start: () => {
    return {
      type: 'OAUTH_START',
    }
  },
  loadUser: () => {
    return {
      type: 'OAUTH_LOAD_USER',
      payload: fetchUser
    }
  },
  setCurrentUser: setCurrentUser,
  login: (username, passwd) => {
    return {
        type: 'LOAD_USER',
        payload: new Promise((resolve, reject) => {
          request.post(`${config.url}${config.urlToken}`, {
            client_id: config.client_id,
            client_secret: config.client_secret,
            grant_type:"password",
            username: username,
            password: passwd,
            scope: "all"
          }).then(res => {
            localStorage.setItem('access_token', res.data.access_token);
            localStorage.setItem('refresh_token', JSON.stringify(res.data.refresh_token));
            fetchUser(res.data.access_token).then((res) => {
              debugger
              localStorage.setItem('current_user', JSON.stringify(res.data));
              resolve(setCurrentUser(res.data));
            });
          }).catch(e => {
            reject(e)
          });
        })
    }
  },
  getToken: (creds, cb) => {
    return dispatch => {
      dispatch(this.start());
      request.post(`${config.url}${config.urlToken}`, Object.assign({
        client_id: config.client_id,
        client_secret: config.client_secret,
        grant_type:"password",
        scope: "all",
      }, creds)).then(res => {
        dispatch(this.complete(res.data, ()=>{
          cb(null, res.data);
        }));
      }).catch(e => {
        cb(e);
        dispatch(this.error(e))
      });
    }
  },
  complete: (token, cb) => {
    return dispatch => {
      // save token
      dispatch(this.saveToken(token));
      // sync user
      dispatch(this.syncUser(token.access_token, cb));
      dispatch({ type: 'OAUTH_COMPLETE' });
    }
  },
  saveToken: (token) => {
    react_cookie.save('redux_oauth2', JSON.stringify(token), {path:'/'});
    return {
      type: 'OAUTH_SAVE_TOKEN',
      payload: token
    }
  },
  signout: () => {
    return dispatch => {
      try {
        let token = react_cookie.load('redux_oauth2');
        request.delete(`${config.url}${config.urlToken}`, {
          headers:{
            'Authorization':`Bearer ${token.access_token}`
          }
        }).then(res => {
          react_cookie.remove('redux_oauth2');
          dispatch({
            type: 'OAUTH_SIGNOUT'
          })
        }).catch(e => dispatch(this.error(e)));
      } catch (e) {
        dispatch(this.error(e))
      }
    }
  },
  syncUser: (token, cb) => {
    return dispatch => {
      fetchUser(token).then(res => {
        dispatch(this.loadUser(res.data))
        cb(res.data);
      }).catch(this.error)
    }
  }
}