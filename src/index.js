import React from 'react';
import ReactDOM from 'react-dom';
import { Router, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import {IntlProvider} from 'react-intl';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';
import Routes from './routes';
import reducers from './reducers';
import setAuthorizationToken from './utils/setAuthorizationToken'
import jwtDecode from 'jwt-decode';
import { setCurrentUser } from './actions/Auth/index';

const middleware = applyMiddleware(thunk,promise(),logger())
const store = createStore(reducers, middleware);

if (localStorage.access_token) {
  setAuthorizationToken(localStorage.access_token);
  const currentUser = JSON.parse(localStorage.current_user);
  store.dispatch(setCurrentUser(currentUser));
} else {
  window.location.replace("#/pages/login");
}

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale="en">
      <Routes />
    </IntlProvider>
  </Provider>
  , document.getElementById('root')
);
