import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Alert, Button, Col, Label, Input, Form, FormGroup, FormText} from 'reactstrap';
import { createUser, updateUser, getPermissions, getPositions } from '../../actions/Users/index';

class UsersForm extends Component {
    constructor (props) {
        super(props);
        var userData = this.props.userData ? this.props.userData : {}
        this.state = {
            dangerAlertOpened: false,
            successAlertOpened: false,
            formData: {
                firstName: userData.firstName,
                lastName: userData.lastName,
                email: userData.email,
                gender: userData.gender ? userData.gender : 'male',
                profilePhoto: userData.image,
                fullAddress: userData.address,
                position: userData.position,
                permission: userData.permission
            },
            positions: this.props.positions
        }
        
        this.toggleAlert = this.toggleAlert.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.props.getPositions();
    }

    handleChange(event) {
        let formData = this.state.formData;
        formData[event.target.id] = event.target.value
        this.setState({formData: formData});
    }

    handleSubmit(event) {
        if(this.props.action === 'create'){
            this.props.createUser(this.state.formData)
            .then((data) => {
                this.toggleAlert('successAlertOpened')
            })
            .catch((error) => {
                this.toggleAlert('dangerAlertOpened')
            })
        }
        if(this.props.action === 'update'){
            this.state.formData.id = this.props.user.id;
            this.props.updateUser(this.state.formData)
            .then((data) => {
                this.toggleAlert('successAlertOpened')
            })
            .catch((error) => {
                this.toggleAlert('dangerAlertOpened')
            })
        }
        event.preventDefault();
    }
    positionOptions = () => {
        var rows = [];
        if (!this.props.positions) return null;
        var length = this.props.positions ? this.props.positions.length : 0;
        for (var i = 0; i < length; i++) {
            rows.push(<option key={i} value={this.props.positions[i].id}>{this.props.positions[i].title}</option>);
        } 
        return rows;
    }
    createPositionsOptions () {
        return (
            <Input type="select" name="position" id="position" value={this.state.formData.position} onChange={this.handleChange}>
                {this.positionOptions()}
            </Input>
        )
    }

    createPermissionsOptions () {
        return (
            <FormGroup tag="fieldset">
                <Label check>
                    <Input type="radio" name="permission" id="permission" value="4" onChange={this.handleChange} checked={"owner" === this.state.formData.permission} />{' '}
                    Owner 
                </Label>
                <Label check>
                    <Input type="radio" name="permission" id="permission" value="3" onChange={this.handleChange} checked={"administrator" === this.state.formData.permission} />{' '}
                    Administrator
                </Label>
                <Label check>
                    <Input type="radio" name="permission" id="permission" value="3" onChange={this.handleChange} checked={"manager" === this.state.formData.permission} />{' '}
                    Manager
                </Label>
                <Label check>
                    <Input type="radio" name="permission" id="permission" value="1" onChange={this.handleChange} checked={"standard" === this.state.formData.permission} />{' '}
                    Standard
                </Label>
            </FormGroup>
        )
    }

    toggleAlert (type) {
        var obj = {};
        obj[type] = !this.state[type]
        this.setState(obj);
    }

    render() {
        return (
            <Form>
                <div>
                    <Alert color="danger" isOpen={this.state.dangerAlertOpened}>
                        <strong>Oh snap!</strong> Change a few things up and try submitting again.
                    </Alert>
                    <Alert color="success" isOpen={this.state.successAlertOpened}>
                        <strong>Saved</strong> information successfully.
                        {' '}<a href='/pages/users/'><i className="fa fa-chevron-left"></i> Users list</a>
                    </Alert>
                </div>
                <FormGroup>
                    <Label for="lblFirstName">First Name:</Label>
                    <Input type="text" name="firstName" id="firstName" value={ this.state.formData.firstName } placeholder="First Name" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="lblLastName">Last Name:</Label>
                    <Input type="text" name="lastName" id="lastName" value={ this.state.formData.lastName } placeholder="Last Name" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="lblEmail">E-mail:</Label>
                    <Input type="email" name="email" id="email" value={ this.state.formData.email } placeholder="anony@tokenlab.com.br" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="lblPhoneNumber">Phone Number:</Label>
                    <Input type="text" name="phoneNumber" id="phoneNumber" value={ this.state.formData.phoneNumber } placeholder="16 3372-3372" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="lblGenderSelect">Gender:</Label>
                    <Input type="select" name="gender" id="gender" value={this.state.formData.gender} onChange={this.handleChange}>
                        <option key="0" value="male">Male</option>
                        <option key="1" value="female">Female</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="lblUserPhoto">Profile Photo:</Label>
                    <Input type="file" name="image" id="image" onChange={this.handleChange} />
                    <FormText color="muted">
                        Use this file form to upload th User profile photo.
                    </FormText>
                </FormGroup>
                <FormGroup>
                    <Label for="lblAddress">Full Address:</Label>
                    <Input type="text" name="fullAddress" id="fullAddress" placeholder="Rua XV de Novembro, Sao Carlos - Brazil" value={this.state.formData.address} onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="lblPositionSelect">Position:</Label>
                    {this.createPositionsOptions()}
                </FormGroup>
                <FormGroup row>
                    <Label for="permissions" sm={2}>Permission:</Label>
                    <Col sm={{ size: 10 }}>
                        {this.createPermissionsOptions()}
                    </Col>
                </FormGroup>
                <FormGroup check row>
                    <Col sm={{ size: 10 }}>
                        <Button onClick={this.handleSubmit}>{this.props.submitBtn ? this.props.submitBtn : 'Submit'}</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.active,
        positions: state.positions
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators({ createUser: createUser, updateUser: updateUser, getPermissions: getPermissions, getPositions: getPositions }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(UsersForm);