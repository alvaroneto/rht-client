import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Alert, Badge, Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router'
import { loadUsersList, selectUser, removeUsers } from '../../actions/Users/index';

class PositionList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.getPositions();
    }
    
    render() {
        var createListItems = () => {
            console.log(this.props.users)
            if(!this.props.users) return(
                <tr key="0">
                    <td colSpan="6">
                        <p>No Positions data.</p>
                    </td>
                </tr>    
            )
            return this.props.positions.map((position, i) => {
                return (<tr key={position.id}>
                    <td>
                        <input type="radio" id="inline-checkbox1" name="inline-checkbox1" value={position.id} onClick={() => this.selectPositionRadio(position)} />
                    </td>
                    <td scope="row">
                        {position.id}
                    </td>
                    <td>
                        {position.title}
                    </td>
                </tr>)
            });
        }
        return (
            <div>
                <div>
                    <Alert color="danger" isOpen={this.state.dangerAlertOpened}>
                        <strong>Oh snap!</strong> Change a few things up and try submitting again.
                    </Alert>
                    <Alert color="success" isOpen={this.state.successAlertOpened}>
                        <strong>Saved</strong> information successfully.
                    </Alert>
                </div>
                <table className="table">
                    <thead className="thead-inverse">
                        <tr>
                            <th>
                                <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropDown}>
                                    <button onClick={this.toggleDropDown} className="btn btn-transparent active dropdown-toggle p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
                                        <i className="icon-settings"></i>
                                    </button>
                                    <DropdownMenu>
                                        <DropdownItem>
                                            <Link to={'/pages/users/new'} className="nav-link">
                                                <i className="icon-plus"></i> Create User
                                            </Link>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <Link to={'/pages/users/edit'} className="nav-link">
                                                <i className="icon-equalizer"></i> Edit User
                                            </Link>
                                        </DropdownItem>
                                        <DropdownItem disabled={ this.state.user ? undefined : true }>
                                            <Link id="remove" onClick={this.toggleRemoveModal} className="nav-link">
                                                <i className="icon-equalizer"></i> Remove Selected 
                                            </Link>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>
                            </th>
                            <th>#</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody>
                        {createListItems()}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators({ loadUsersList: loadUsersList, selectUser: selectUser, removeUsers: removeUsers }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(PositionList);