import { combineReducers } from "redux";
import UserReducer from './user.reducer';
import UsersReducer from './users.reducer';
import PositionsReducer from './positions.reducer';
import AuthReducer from './auth.reducer'

const allReducers = combineReducers({
    users: UsersReducer,
    active: UserReducer,
    positions: PositionsReducer,
    auth: AuthReducer
})

export default allReducers;