export default (state=null, action) => {
  switch(action.type) {
    case "FETCH_POSITIONS":
        return action.payload;
    case "FETCH_POSITIONS_FULFILLED":
        return action.payload;
    default:
        return state;
  }
}
